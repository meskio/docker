# Todo:
#   - Use debian-buster component for https://download.docker.com/ once it's available
#

FROM debian:buster

MAINTAINER LEAP Encryption Access Project <sysdev@leap.se>
LABEL Description="Base debian buster baseimage with few customisation" Vendor="LEAP" Version="1.x"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
  && apt-get -y dist-upgrade \
  && apt-get install -y --no-install-recommends \
    git \
    leap-archive-keyring \
    locales \
    ca-certificates \
    curl \
    openssh-client \
    gnupg \
    gnupg2 \
    software-properties-common \
    python \
    ca-certificates \
    sudo \
    apt-transport-https \
    golang-go \
    golang-golang-x-text-dev \
    libgtk-3-dev \
    libappindicator3-dev \
    debhelper dh-golang \
    build-essential \
    devscripts \
    pkg-config \
    git-buildpackage \
  && apt-get clean

RUN echo 'deb https://download.docker.com/linux/debian buster stable'> /etc/apt/sources.list.d/docker.list
RUN curl -sS https://download.docker.com/linux/debian/gpg | apt-key add -
RUN apt-get update \
  && apt-get -y dist-upgrade \
  && apt-get -y install docker-engine


RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN id cirunner || adduser --group --system --shell /bin/bash cirunner

COPY files/etc /etc/
