FROM debian:stretch

MAINTAINER LEAP Encryption Access Project <sysdev@leap.se>
LABEL Description="Base debian stretch baseimage with few customisation" Vendor="LEAP" Version="1.x"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
  && apt-get -y dist-upgrade \
  && apt-get install -y --no-install-recommends \
    git \
    leap-archive-keyring \
    locales \
    ca-certificates \
    curl \
    openssh-client \
    gnupg2 \
    software-properties-common \
    python \
    ca-certificates \
    sudo \
    curl \
    gnupg \
    apt-transport-https && \
    apt-get clean

RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

# Platform doesn't have a stretch repo yet
# RUN echo "deb [signed-by=/usr/share/keyrings/leap-archive.gpg] http://deb.leap.se/platform staging stretch" > /etc/apt/sources.list.d/leap.list
RUN echo 'deb https://download.docker.com/linux/debian stretch stable'> /etc/apt/sources.list.d/docker.list
RUN curl -sS https://download.docker.com/linux/debian/gpg | apt-key add -
RUN apt-get update \
  && apt-get -y dist-upgrade
RUN apt-get -y install docker-engine

RUN id cirunner || useradd -ms /bin/bash -G sudo cirunner
RUN adduser cirunner docker
# allow all members of sudo group to execute any command without password
RUN sed -i 's/^%sudo.*/%sudo ALL=(ALL) NOPASSWD:ALL/' /etc/sudoers

COPY files/etc /etc/
